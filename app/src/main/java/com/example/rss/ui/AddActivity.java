package com.example.rss.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rss.R;
import com.example.rss.model.Site;
import com.example.rss.network.ApiRestClient;
import com.example.rss.network.ApiTokenRestClient;
import com.example.rss.util.SharedPreferencesManager;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity implements Callback<Site> {
    public static final int OK = 1;

    @BindView(R.id.nameSite) EditText nameSite;
    @BindView(R.id.linkSite) EditText linkSite;
    @BindView(R.id.emailSite) EditText emailSite;
    @BindView(R.id.accept) Button accept;
    @BindView(R.id.cancel) Button cancel;

    ProgressDialog progreso;
    SharedPreferencesManager preferences;

    @OnClick(R.id.accept)
    public void clickAccept(View view){
        String n, l, e;
        Site s;

        hideSoftKeyboard();
        n = nameSite.getText().toString();
        l = linkSite.getText().toString();
        e = emailSite.getText().toString();
        if (n.isEmpty() || l.isEmpty())
            Toast.makeText(this, "Please, fill the name and the link", Toast.LENGTH_SHORT).show();
        else {
            s = new Site(n, l , e);
            connection(s);
        }
    }

    @OnClick(R.id.cancel)
    public void clickCancel(View view){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ButterKnife.bind(this);
        preferences = new SharedPreferencesManager(this);
    }

    private void connection(Site s) {
        progreso = new ProgressDialog(this);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Connecting . . .");
        progreso.setCancelable(false);
        progreso.show();

        //Call<Site> call = ApiRestClient.getInstance().createSite("Bearer " + preferences.getToken(), s);
        Call<Site> call = ApiTokenRestClient.getInstance(preferences.getToken()).createSite(s);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<Site> call, Response<Site> response) {
        progreso.dismiss();
        if (response.isSuccessful()) {
            Site site = response.body();
            Intent i = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("id", site.getId());
            bundle.putString("name", site.getName());
            bundle.putString("link", site.getLink());
            bundle.putString("email", site.getEmail());
            i.putExtras(bundle);
            setResult(OK, i);
            finish();
            showMessage("Added site ok");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Download error: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<Site> call, Throwable t) {
        progreso.dismiss();
        if (t != null)
            showMessage("Failure in the communication\n" + t.getMessage());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}

